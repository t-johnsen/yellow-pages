﻿using System;
using System.Collections.Generic;

namespace yellow_pages
{
    class Program
    {
        static void Main(string[] args)
        {
            
            List<string> names = new List<string> { "Mari Teiler-Johnsen", "Rinsai Moontika", "Aksel Andresen", "Live Andresen", "Erling Teiler-Johnsen" };

            Console.WriteLine("Yellow pages containing the following names:");
            Print(names);

            Console.WriteLine("\nInsert something you want to search for, please.");
            string searchString;
            searchString = Console.ReadLine();

            List<string> result = Search(searchString, names);

            Console.WriteLine($"\nResult for search containing '{searchString}'\n");
            Print(result);

        }

        // Print method for the list containing names
        private static void Print(List<string> names)
        {
            foreach (string name in names)
            {
                Console.WriteLine(name);
            }
        }

        // Search for name (both full and partial) in list. Not case sensitive
        private static List<string> Search(string searchString, List<string> names)
        {
            return names.FindAll(name => name.ToLower().Contains(searchString.ToLower()));
        }
    }
}
